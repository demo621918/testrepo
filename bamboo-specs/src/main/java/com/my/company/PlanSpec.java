package com.my.company;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepository;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.api.builders.permission.ProjectPermissions;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@BambooSpec
public class PlanSpec {
    /**
     * Run main to publish plan on Bamboo
     */
    public static void main(final String[] args) throws Exception {
       //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("http://localhost:8085");
        Project proj = new PlanSpec().project();
        ProjectPermissions currentProjectPermissions = new PlanSpec().createProjectPermissions(String.valueOf(proj.getKey()));
        bambooServer.publish(currentProjectPermissions);
    }

    PlanPermissions createPlanPermission(PlanIdentifier planIdentifier) {
        Permissions permission = new Permissions()
                .userPermissions("admin", PermissionType.ADMIN, PermissionType.CLONE, PermissionType.EDIT)
                .groupPermissions("bamboo-admin", PermissionType.ADMIN)
                .loggedInUserPermissions(PermissionType.ADMIN)
                .anonymousUserPermissionView();
        return new PlanPermissions(planIdentifier.getProjectKey(), planIdentifier.getPlanKey()).permissions(permission);
    }

    Project project() {
        return new Project()
                .name("poc")
                .key("POC");
    }

    ProjectPermissions createProjectPermissions(String projectKey) {
        Permissions permission = new Permissions()
                .userPermissions("admin", PermissionType.ADMIN)
                .groupPermissions("bamboo-admin", PermissionType.ADMIN)
                .loggedInUserPermissions(PermissionType.ADMIN)
                .anonymousUserPermissionView();
        return new ProjectPermissions(projectKey).projectPermissions(permission);
    }

    Plan createPlan() {
        return new Plan(
                project(),
                "demo plan", "DP")
                .description("Plan created by user")
                .planRepositories(
                )
                .stages(
                        new Stage("Stage 1").jobs(
                                new Job("Job Name", "JOBKEY")
                                        .tasks(
                                                scriptTask()
                                        )
                                        .artifacts(artifact())
                        )
                );
    }

    VcsCheckoutTask gitRepositoryCheckoutTask() {
        return new VcsCheckoutTask()
                .addCheckoutOfDefaultRepository();
    }

    ScriptTask scriptTask() {
        return new ScriptTask()
                .inlineBody("mkdir target; echo 'hello world' > target/console.out")
                .interpreterShell();
    }

    Artifact artifact() {
        return new Artifact("Build results")
                .location("target")
                .copyPattern("**/*");
    }

}

